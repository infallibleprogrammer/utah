/*
#include <stdio.h>
#include <string.h>

#include "unity.h"

#include "Mockboard_io.h"
#include "Mockgpio.h"

#include "Mockadc.h"
#include "Mockclock.h"
#include "Mockdelay.h"
#include "Mocksys_time.h"
#include "ultra_sonic_sensors.c"

void setUp(void) {}

void tearDown(void) {}

void test__ultra_sonic_sensors_initialize_trigger_pins(void) {
  gpio_s gpio = {};
  gpio__construct_as_output_ExpectAndReturn(GPIO__PORT_0, 5, gpio);
  gpio__construct_as_output_ExpectAndReturn(GPIO__PORT_0, 7, gpio);
  gpio__construct_as_output_ExpectAndReturn(GPIO__PORT_0, 8, gpio);
  gpio__construct_as_output_ExpectAndReturn(GPIO__PORT_0, 9, gpio);

  gpio__set_function_Expect(front_ultra_sonic_trigger, GPIO__FUNCITON_0_IO_PIN);
  gpio__set_function_Expect(back_ultra_sonic_trigger, GPIO__FUNCITON_0_IO_PIN);
  gpio__set_function_Expect(right_ultra_sonic_trigger, GPIO__FUNCITON_0_IO_PIN);
  gpio__set_function_Expect(left_ultra_sonic_trigger, GPIO__FUNCITON_0_IO_PIN);

  gpio__set_Expect(front_ultra_sonic_trigger);
  gpio__set_Expect(back_ultra_sonic_trigger);
  gpio__set_Expect(right_ultra_sonic_trigger);
  gpio__set_Expect(left_ultra_sonic_trigger);
  delay__ms_Expect(500);

  gpio__reset_Expect(front_ultra_sonic_trigger);
  gpio__reset_Expect(back_ultra_sonic_trigger);
  gpio__reset_Expect(right_ultra_sonic_trigger);
  gpio__reset_Expect(left_ultra_sonic_trigger);

  initialize_pins_for_ultra_sonic_sensor_triggers();
}

void test__fill_left_and_right_ultra_sonic_distance_buffer_expected_function_calls(void) {
  size_t index = 0;
  uint16_t test_adc_return_values[BUFFER_SIZE];

  gpio__set_Expect(left_ultra_sonic_trigger);
  delay__us_Expect(sensor_trigger_delay);
  for (index = 0; index < BUFFER_SIZE; index++) {
    adc__get_adc_value_ExpectAndReturn(ADC__CHANNEL_2, 150);
  }
  gpio__reset_Expect(left_ultra_sonic_trigger);

  gpio__set_Expect(right_ultra_sonic_trigger);
  delay__us_Expect(sensor_trigger_delay);
  for (index = 0; index < BUFFER_SIZE; index++) {
    adc__get_adc_value_ExpectAndReturn(ADC__CHANNEL_5, 150);
  }
  gpio__reset_Expect(right_ultra_sonic_trigger);

  fill_left_and_right_ultra_sonic_distance_buffer();
}

void test__fill_front_and_back_ultra_sonic_distance_buffer_expected_function_calls(void) {
  size_t index = 0;
  uint16_t test_adc_return_values[BUFFER_SIZE];

  gpio__set_Expect(front_ultra_sonic_trigger);
  delay__us_Expect(sensor_trigger_delay);
  for (index = 0; index < BUFFER_SIZE; index++) {
    adc__get_adc_value_ExpectAndReturn(ADC__CHANNEL_4, index);
  }
  gpio__reset_Expect(front_ultra_sonic_trigger);

  gpio__set_Expect(back_ultra_sonic_trigger);
  delay__us_Expect(sensor_trigger_delay);
  for (index = 0; index < BUFFER_SIZE; index++) {
    adc__get_adc_value_ExpectAndReturn(ADC__CHANNEL_3, index);
  }
  gpio__reset_Expect(back_ultra_sonic_trigger);

  fill_front_and_back_ultra_sonic_distance_buffer();
}

void test__median_val(void) {
  size_t index = BUFFER_SIZE;
  size_t median_position = (BUFFER_SIZE) / 2;

  for (index = 0; index < BUFFER_SIZE; index++) {
    if (index != median_position) {
      left_sensor_distance_inches[index] = index;
    } else {
      // Set middle index of array to the highest value in the array
      left_sensor_distance_inches[index] = BUFFER_SIZE + 1;
    }
  }
  sort_ultra_sonic_distances(LEFT_ULTRA_SONIC);

  uint16_t median = get_median_distance(LEFT_ULTRA_SONIC);
  uint16_t expected_val = median_position + 1;

  TEST_ASSERT_EQUAL(expected_val, median);
}
*/