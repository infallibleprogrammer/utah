#include "unity.h"
#include <stdio.h>
#include <string.h>

// Include the source we wish to test
#include "Mockcan_bus.h"
#include "can_bus_initializer.h"

void setUp(void) {}

void tearDown(void) {}

void test__initiailize_can1_bus(void) {

  can__init_ExpectAndReturn(can1, 100, 100, 100, NULL, NULL, true);

  can__bypass_filter_accept_all_msgs_Expect();

  can__reset_bus_Expect(can1);
  initiailize_can1_bus(100, 100, 100);
}

void test__initiailize_can2_bus(void) {
  can__init_ExpectAndReturn(can2, 100, 100, 100, NULL, NULL, true);
  can__bypass_filter_accept_all_msgs_Expect();
  can__reset_bus_Expect(can2);
  initiailize_can2_bus(100, 100, 100);
}
