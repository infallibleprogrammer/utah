#include <stdio.h>
#include <string.h>

#include "unity.h"

#include "Mockboard_io.h"
#include "Mockcan_bus.h"
#include "Mockgpio.h"
#include "Mockqueue.h"

#include "Mockcan_bus_initializer.h"
#include "can_dbc_send_msg.c"

#include "Mockclock.h"
#include "line_buffer.h"

#include "Mockuart.h"

#include "Mocksensor_controller.h"
#include "bridge_controller.c"
#include "fake_gps_data.c"
#include "project.h"
/*
const uint32_t dbc_mia_threshold_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG = 600;
const uint32_t dbc_mia_threshold_COMPASS_HEADING_DISTANCE = 1500;
const uint32_t dbc_mia_threshold_GPS_CURRENT_INFO = 1500;
const uint32_t dbc_mia_threshold_GPS_CURRENT_DESTINATIONS_DATA = 1500;
const uint32_t dbc_mia_threshold_RC_CAR_SPEED_READ_MSG = 15000;
*/
void setUp(void) {
  gpio_s test_gpio;
  uint32_t test_clock;
  QueueHandle_t rxq_handle;
  QueueHandle_t txq_handle;
  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_4, 28, GPIO__FUNCTION_2, test_gpio);
  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_4, 29, GPIO__FUNCTION_2, test_gpio);
  clock__get_peripheral_clock_hz_ExpectAndReturn(test_clock);
  uart__init_Expect(bridge_uart, test_clock, 9600);
  xQueueCreate_ExpectAndReturn(50, sizeof(char), rxq_handle);
  xQueueCreate_ExpectAndReturn(50, sizeof(char), txq_handle);
  uart__enable_queues_ExpectAndReturn(bridge_uart, rxq_handle, txq_handle, true);
  Bridge_Controller_init();
}

void tearDown(void) {}

void test__bridge_controller(void) {}

void test__get_Bridge_Controller__get_coordinates_should_return_true(void) {

  gps_destination_location_last_sent.DEST_LONGITUDE = 150.123456;
  gps_destination_location_last_sent.DEST_LATITUDE = 90.123456;
  dbc_GPS_DESTINATION_LOCATION_s gps_test_send_data = {.DEST_LONGITUDE = 132.123456, .DEST_LATITUDE = 85.123456};

  TEST_ASSERT_TRUE(Bridge_Controller__get_coordinates(&gps_test_send_data));
}

void test__get_Bridge_Controller__get_coordinates_should_return_false(void) {
  gps_destination_location_last_sent.DEST_LONGITUDE = 150.123456;
  gps_destination_location_last_sent.DEST_LATITUDE = 90.123456;
  dbc_GPS_DESTINATION_LOCATION_s gps_test_send_data = {.DEST_LONGITUDE = 150.123456, .DEST_LATITUDE = 90.123456};

  TEST_ASSERT_FALSE(Bridge_Controller__get_coordinates(&gps_test_send_data));
}

void test__Bridge_Controller__1hz_handler__with_fake_gps_data(void) {
  size_t count;
  size_t send_same_data_count = 20;
  float expected_fake_data_dest_longitude;
  float expected_fake_data_dest_latitude;
  dbc_GPS_DESTINATION_LOCATION_s test_gps_send_data = {};

  // Fake_gps sends new data every 20 iterations, they are predefined values
  // Bridge_controller should not send data if no new data on can__tx
  can__tx_ExpectAndReturn(can1, &tx_msg, 0, true);
  Bridge_Controller__1hz_handler();
  expected_fake_data_dest_longitude = -121.881000;
  expected_fake_data_dest_latitude = 37.336000;

  dbc_decode_GPS_DESTINATION_LOCATION(&test_gps_send_data, dbc_header_GPS_DESTINATION_LOCATION, tx_msg.data.bytes);
  TEST_ASSERT_FLOAT_WITHIN(0.0001, test_gps_send_data.DEST_LONGITUDE, expected_fake_data_dest_longitude);
  TEST_ASSERT_FLOAT_WITHIN(0.0001, test_gps_send_data.DEST_LATITUDE, expected_fake_data_dest_latitude);

  for (count = 0; count < send_same_data_count - 1; count++) {
    Bridge_Controller__1hz_handler();
    TEST_ASSERT_FLOAT_WITHIN(0.0001, test_gps_send_data.DEST_LONGITUDE, expected_fake_data_dest_longitude);
    TEST_ASSERT_FLOAT_WITHIN(0.0001, test_gps_send_data.DEST_LATITUDE, expected_fake_data_dest_latitude);
  }

  expected_fake_data_dest_longitude = -121.882000;
  expected_fake_data_dest_latitude = 37.337000;

  can__tx_ExpectAndReturn(can1, &tx_msg, 0, true);
  Bridge_Controller__1hz_handler();

  dbc_decode_GPS_DESTINATION_LOCATION(&test_gps_send_data, dbc_header_GPS_DESTINATION_LOCATION, tx_msg.data.bytes);
  TEST_ASSERT_FLOAT_WITHIN(0.0001, test_gps_send_data.DEST_LONGITUDE, expected_fake_data_dest_longitude);
  TEST_ASSERT_FLOAT_WITHIN(0.0001, test_gps_send_data.DEST_LATITUDE, expected_fake_data_dest_latitude);

  for (count = 0; count < send_same_data_count - 1; count++) {
    Bridge_Controller__1hz_handler();
    dbc_decode_GPS_DESTINATION_LOCATION(&test_gps_send_data, dbc_header_GPS_DESTINATION_LOCATION, tx_msg.data.bytes);
    TEST_ASSERT_FLOAT_WITHIN(0.0001, test_gps_send_data.DEST_LONGITUDE, expected_fake_data_dest_longitude);
    TEST_ASSERT_FLOAT_WITHIN(0.0001, test_gps_send_data.DEST_LATITUDE, expected_fake_data_dest_latitude);
  }

  expected_fake_data_dest_longitude = -121.883000;
  expected_fake_data_dest_latitude = 37.338000;

  can__tx_ExpectAndReturn(can1, &tx_msg, 0, true);
  Bridge_Controller__1hz_handler();

  dbc_decode_GPS_DESTINATION_LOCATION(&test_gps_send_data, dbc_header_GPS_DESTINATION_LOCATION, tx_msg.data.bytes);
  TEST_ASSERT_FLOAT_WITHIN(0.0001, test_gps_send_data.DEST_LONGITUDE, expected_fake_data_dest_longitude);
  TEST_ASSERT_FLOAT_WITHIN(0.0001, test_gps_send_data.DEST_LATITUDE, expected_fake_data_dest_latitude);

  for (count = 0; count < send_same_data_count - 1; count++) {
    Bridge_Controller__1hz_handler();
    dbc_decode_GPS_DESTINATION_LOCATION(&test_gps_send_data, dbc_header_GPS_DESTINATION_LOCATION, tx_msg.data.bytes);
    TEST_ASSERT_FLOAT_WITHIN(0.0001, test_gps_send_data.DEST_LONGITUDE, expected_fake_data_dest_longitude);
    TEST_ASSERT_FLOAT_WITHIN(0.0001, test_gps_send_data.DEST_LATITUDE, expected_fake_data_dest_latitude);
  }

  expected_fake_data_dest_longitude = -121.886000;
  expected_fake_data_dest_latitude = 37.340000;

  can__tx_ExpectAndReturn(can1, &tx_msg, 0, true);
  Bridge_Controller__1hz_handler();

  dbc_decode_GPS_DESTINATION_LOCATION(&test_gps_send_data, dbc_header_GPS_DESTINATION_LOCATION, tx_msg.data.bytes);
  TEST_ASSERT_FLOAT_WITHIN(0.0001, test_gps_send_data.DEST_LONGITUDE, expected_fake_data_dest_longitude);
  TEST_ASSERT_FLOAT_WITHIN(0.0001, test_gps_send_data.DEST_LATITUDE, expected_fake_data_dest_latitude);

  for (count = 0; count < send_same_data_count - 1; count++) {
    Bridge_Controller__1hz_handler();
    dbc_decode_GPS_DESTINATION_LOCATION(&test_gps_send_data, dbc_header_GPS_DESTINATION_LOCATION, tx_msg.data.bytes);
    TEST_ASSERT_FLOAT_WITHIN(0.0001, test_gps_send_data.DEST_LONGITUDE, expected_fake_data_dest_longitude);
    TEST_ASSERT_FLOAT_WITHIN(0.0001, test_gps_send_data.DEST_LATITUDE, expected_fake_data_dest_latitude);
  }
}

void test__Bridge_Controller__10hz_handler__BT_send_uint32_t(void) {
  uint32_t test_uint32 = 32;
  uint32_t test_command = 4;

  char test_name_send[32];
  sprintf(test_name_send, "%d%d\n", test_command, test_uint32);

  for (int i = 0; i < sizeof(test_name_send); i++) {
    uart__put_ExpectAndReturn(bridge_uart, test_name_send[i], 0, true);

    if (test_name_send[i] == '\n')
      break;
  }
  Bridge_Controller__Bluetooth_Send_uint32(test_uint32, test_command);
}
void test__Bridge_Controller__10hz_handler__BT_send_float(void) {

  float test_uint32 = 32.421;
  uint32_t test_command = 4;

  char test_name_send[32];
  sprintf(test_name_send, "%d%2.4f\n", test_command, test_uint32);

  for (int i = 0; i < sizeof(test_name_send); i++) {
    uart__put_ExpectAndReturn(bridge_uart, test_name_send[i], 0, true);

    if (test_name_send[i] == '\n')
      break;
  }
  Bridge_Controller__Bluetooth_Send_float(test_uint32, test_command);
}