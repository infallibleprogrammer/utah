#include "can_dbc_send_msg.h"

static can__msg_t tx_msg = {};
bool dbc_send_can_message(void *argument_from_dbc_encode_and_send, uint32_t message_id, const uint8_t bytes[8],
                          uint8_t dlc) {

  memcpy(tx_msg.data.bytes, bytes, 8);

  tx_msg.msg_id = message_id;
  tx_msg.frame_fields.data_len = dlc;
  bool success = can__tx(can1, &tx_msg, 0);
  return success;
}
