#include "periodic_callbacks.h"

#include "board_io.h"
#include "gpio.h"
#include "gps.h"

#include "can_bus_initializer.h"
#include "can_module.h"
#include "compass.h"
#include "delay.h"
#include "geo_logic.h"

/******************************************************************************
 * Your board will reset if the periodic function does not return within its deadline
 * For 1Hz, the function must return within 1000ms
 * For 1000Hz, the function must return within 1ms
 */
void periodic_callbacks__initialize(void) {
  compass__init();
  can_bus_initializer();
  gpio__set(board_io__get_led0());
  gpio__set(board_io__get_led1());
  gpio__set(board_io__get_led2());
  gpio__set(board_io__get_led3());
  gps__init();
}

void periodic_callbacks__1Hz(uint32_t callback_count) {
  compass__run_once_get_heading();
  print_compass_run_once();
  periodic_callbacks_1Hz_GEO_commands();
  gps__setup_command_registers();
}

void periodic_callbacks__10Hz(uint32_t callback_count) {
  gps__run_once();
  if (callback_count % 2 == 0) {
    periodic_callbacks_10Hz_GEO_commands();
  }
  if (callback_count % 3 == 0) {
    can_bus_handler__process_all_received_messages();
  }
}
void periodic_callbacks__100Hz(uint32_t callback_count) {
  gpio__toggle(board_io__get_led2());
  gps__run_once();
  // Add your code here
}

/**
 * @warning
 * This is a very fast 1ms task and care must be taken to use this
 * This may be disabled based on intialization of periodic_scheduler__initialize()
 */
void periodic_callbacks__1000Hz(uint32_t callback_count) {
  // gpio__toggle(board_io__get_led3());
  // Add your code here
}
