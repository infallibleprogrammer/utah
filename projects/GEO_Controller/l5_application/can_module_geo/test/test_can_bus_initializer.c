#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockcan_bus.h"

#include "can_bus_initializer.h"

void test_can_bus_initalizer_successful(void) {
  bool was_can_initialization_successful = false;
  const can__num_e CAN_PORT2 = can1;
  const uint32_t baud_rate_in_kbps = 200;
  const uint16_t rx_queue_size = 200;
  const uint16_t tx_queue_size = 100;

  can__init_ExpectAndReturn(CAN_PORT2, baud_rate_in_kbps, rx_queue_size, tx_queue_size, NULL, NULL, true);
  can__bypass_filter_accept_all_msgs_Expect();
  can__reset_bus_Expect(CAN_PORT2);
  TEST_ASSERT_TRUE(can_bus_initializer());
}

void test_can_bus_initalizer_invalid_can_port(void) {
  bool was_can_initialization_successful = false;
  const can__num_e CAN_MAX = can1;
  const uint32_t baud_rate_in_kbps = 200;
  const uint16_t rx_queue_size = 200;
  const uint16_t tx_queue_size = 100;

  can__init_ExpectAndReturn(CAN_MAX, baud_rate_in_kbps, rx_queue_size, tx_queue_size, NULL, NULL, false);
  TEST_ASSERT_FALSE(can_bus_initializer());
}
