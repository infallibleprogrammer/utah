#include "unity.h"

#include <stdint.h>
#include <stdio.h>
#include <string.h>
// Mocks
#include "Mockboard_io.h"
#include "Mockclock.h"
#include "Mockgpio.h"
#include "Mocki2c.h"

#include "Mockqueue.h"

#include "compass.c"

void test_compass__init(void) {
  const uint8_t mag_register_value_setup[3] = {0x90, 0x00, 0x00};
  const uint32_t i2c_speed_hz = UINT32_C(400) * 1000;
  uint32_t clock_return;
  gpio_s compass_data_available = {0, 22};
  clock__get_peripheral_clock_hz_ExpectAndReturn(clock_return);
  i2c__initialize_ExpectAnyArgs();
  i2c__write_single_ExpectAnyArgsAndReturn(1);
  i2c__write_single_ExpectAnyArgsAndReturn(1);
  i2c__write_single_ExpectAnyArgsAndReturn(1);     // from gain
  i2c__write_slave_data_ExpectAnyArgsAndReturn(1); // from gain
  gpio__set_as_input_Expect(compass_data_available);
  gpio__enable_pull_down_resistors_Expect(compass_data_available);
  compass__init();
}

void test_get_raw_compass_data() {
  i2c__read_slave_data_ExpectAnyArgsAndReturn(1);
  get_raw_compass_data();
}

void test_get_current_compass_heading_negative_x_negative_y() {
  magnetometer_processed.x = -500.0;
  magnetometer_processed.y = -460.0;
  magnetometer_processed.z = 0.0;
  accel_raw_data.x = 50;
  accel_raw_data.y = 100;
  accel_raw_data.z = 200;
  float compass_value_expected = 222.2986;
  get_current_compass_heading();
  TEST_ASSERT_EQUAL_FLOAT(compass_value_expected, current_compass_heading);
}

void test_get_current_compass_heading_positive_x_negative_y() {
  magnetometer_processed.x = 175.0;
  magnetometer_processed.y = -460.0;
  magnetometer_processed.z = 0.0;
  accel_raw_data.x = 50;
  accel_raw_data.y = 100;
  accel_raw_data.z = 200;
  float compass_value_expected = 262.1183;
  get_current_compass_heading();
  TEST_ASSERT_EQUAL_FLOAT(compass_value_expected, current_compass_heading);
}

void test_get_current_compass_heading_positive_x_positive_y() {
  magnetometer_processed.x = 175.0;
  magnetometer_processed.y = 360.0;
  magnetometer_processed.z = 0.0;
  accel_raw_data.x = 50;
  accel_raw_data.y = 100;
  accel_raw_data.z = 200;
  float compass_value_expected = 327.1405;
  get_current_compass_heading();
  TEST_ASSERT_EQUAL_FLOAT(compass_value_expected, current_compass_heading);
}

void test_get_current_compass_heading_negative_x_positive_y() {
  magnetometer_processed.x = -2000.0;
  magnetometer_processed.y = 360.0;
  magnetometer_processed.z = 0.0;
  accel_raw_data.x = 50;
  accel_raw_data.y = 100;
  accel_raw_data.z = 200;
  float compass_value_expected = 174.5049;
  get_current_compass_heading();
  TEST_ASSERT_EQUAL_FLOAT(compass_value_expected, current_compass_heading);
}

void test_gain_set() {
  compass_magnetometer_gain_values_e gain = 0x20;
  i2c__write_single_ExpectAnyArgsAndReturn(1);
  set_magnetometer_gain(gain);
}

void test_compass__run_once_get_heading(void) {
  float compass_value_expected = 169.5773;
  magnetometer_processed.x = -2000.0;
  magnetometer_processed.y = 360.0;
  magnetometer_processed.z = 0.0;
  accel_raw_data.x = 50;
  accel_raw_data.y = 100;
  accel_raw_data.z = 200;
  get_current_compass_heading();
  TEST_ASSERT_EQUAL_FLOAT(compass_value_expected, current_compass_heading);
}