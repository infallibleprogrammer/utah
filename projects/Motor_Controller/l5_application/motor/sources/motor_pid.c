/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/
/* Main Module Header */
#include "motor_pid.h"

/* Standard Includes */
#include <stddef.h>

/* External Includes */

/***********************************************************************************************************************
 *
 *                                                   D E F I N E S
 *
 **********************************************************************************************************************/
#define HISTORY_BUF_SIZE 5

/***********************************************************************************************************************
 *
 *                                  P R I V A T E   D A T A   D E F I N I T I O N S
 *
 **********************************************************************************************************************/
static const double Kp = 0.5;
static const double Ki = 0.05;
static const double Kd = 0;

static double error_history[HISTORY_BUF_SIZE] = {0};

static double dt = 0.5f; // Sample rate of 2Hz (maybe faster is needed)

/***********************************************************************************************************************
 *
 *                                         P R I V A T E   F U N C T I O N S
 *
 **********************************************************************************************************************/
static void update_error_buffer(double latest_error) {

  for (size_t i = HISTORY_BUF_SIZE - 1; i > 0; --i) {
    error_history[i] = error_history[i - 1];
  }
  error_history[0] = latest_error;
}

static double error_central_difference(void) { return (error_history[0] - error_history[2]) / 2; }

static double squared_error_integrate(void) {
  double integral = 0.0;

  for (size_t i = 0; i < HISTORY_BUF_SIZE; ++i) {
    integral += error_history[i] * error_history[i];
  }

  return integral;
}

/***********************************************************************************************************************
 *
 *                                          P U B L I C   F U N C T I O N S
 *
 **********************************************************************************************************************/
double motor_pid__compute_pid(double target_speed, double actual_speed) {

  update_error_buffer(target_speed - actual_speed);

  /**
   * Time delta will probably need to be added somewhere
   */
  return Kp * error_history[0] + Ki * squared_error_integrate() + Kd * error_central_difference();
}
