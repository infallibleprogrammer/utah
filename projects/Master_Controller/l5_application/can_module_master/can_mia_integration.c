#include "project.h"

#include <stdint.h>

#include "can_mia_integration.h"

const uint32_t dbc_mia_threshold_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG = 500;

// Car Stops with uninitalized value
const dbc_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG_s dbc_mia_replacement_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG;