#include "fake_gps_data.h"

// 37.33698992346661, -121.88190106567058 Next to garage
// 37.338895,         -121.881153
static dbc_GPS_DESTINATION_LOCATION_s data_to_send;
static uint32_t fake_gps_send_counter;

void fake_gps_init(void) {
  data_to_send.DEST_LONGITUDE = -121.881901;
  data_to_send.DEST_LATITUDE = 37.336989;
}

dbc_GPS_DESTINATION_LOCATION_s *fake_gps_data(void) {
  if (fake_gps_send_counter < 20) {
    data_to_send.DEST_LONGITUDE = -121.881000;
    data_to_send.DEST_LATITUDE = 37.336000;
  } else if (fake_gps_send_counter < 40) {
    data_to_send.DEST_LONGITUDE = -121.882000;
    data_to_send.DEST_LATITUDE = 37.337000;
  } else if (fake_gps_send_counter < 60) {
    data_to_send.DEST_LONGITUDE = -121.883000;
    data_to_send.DEST_LATITUDE = 37.338000;
  } else if (fake_gps_send_counter < 80) {
    data_to_send.DEST_LONGITUDE = -121.886000;
    data_to_send.DEST_LATITUDE = 37.340000;
  } else {
  }
  fake_gps_send_counter = (fake_gps_send_counter + 1) % 80;
  return &data_to_send;
}
