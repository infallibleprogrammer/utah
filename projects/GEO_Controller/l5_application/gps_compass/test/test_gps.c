#include "unity.h"

#include <stdint.h>
#include <stdio.h>
#include <string.h>
// Mocks
#include "Mockboard_io.h"
#include "Mockclock.h"
#include "Mockgpio.h"
#include "Mockuart.h"

#include "Mockqueue.h"

// Use the real implementation (not mocks) for:
#include "line_buffer.h"

// Include the source we wish to test
#include "gps.h"

static const uint32_t peripheral_clock = 96000000;
static const uint32_t baud_rate = 9600;

void setUp(void) {}
void tearDown(void) {}

void test_init(void) {
  QueueHandle_t rxq_handle, txq_handle;
  gpio_s gpio = {};
  clock__get_peripheral_clock_hz_ExpectAndReturn(peripheral_clock);
  uart__init_Expect(UART__3, peripheral_clock, baud_rate);
  xQueueCreate_ExpectAndReturn(50, sizeof(char), rxq_handle);
  xQueueCreate_ExpectAndReturn(8, sizeof(char), txq_handle);
  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_4, 28, GPIO__FUNCTION_2, gpio);
  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_4, 29, GPIO__FUNCTION_2, gpio);
  uart__enable_queues_ExpectAndReturn(UART__3, rxq_handle, txq_handle, true);
  gps__init();
}

void test_GPGLL_line_is_ignored(void) {
  char *GPGLL_data = "$GPGLL,4916.45,N,12311.12,W,225444,A\n";
  float longitude_data = 4916.45;
  float latitude_data = -12311.12;
  gpio_s switchs;
  gpio_s led = {};
  gpio_s data_ready = {GPIO__PORT_0, 6};
  gps_coordinates_t parsed_coordinates;

  test_init();

  for (int i = 0; i < strlen(GPGLL_data); i++) {
    uart__get_ExpectAndReturn(UART__3, NULL, 0, true);
    uart__get_IgnoreArg_input_byte();
    uart__get_ReturnThruPtr_input_byte(&GPGLL_data[i]);
  }
  uart__get_ExpectAnyArgsAndReturn(false);
  gps__run_once();
  parsed_coordinates = gps__get_coordinates();
  TEST_ASSERT_NOT_EQUAL(longitude_data, parsed_coordinates.latitude);
  TEST_ASSERT_NOT_EQUAL(latitude_data, parsed_coordinates.longitude);
}

void test_GPGGA_coordinates_are_parsed(void) {
  char *uart_driver_returned_data =
      "$GPGGA,134658.00,5106.9792,N,11402.3003,E,2,09,1.0,1048.47,M,-16.27,M,08,AAAA*60\n";
  float longitude_data = 5106.9792;
  float longitude_data_degrees = 51.11632;
  float latitude_data = 11402.3003;
  float latitude_data_degrees = 114.0383;
  gps_coordinates_t parsed_coordinates;
  gpio_s switchs;
  gpio_s led = {};
  gpio_s data_ready = {GPIO__PORT_0, 6};

  test_init();

  for (size_t index = 0; index <= strlen(uart_driver_returned_data); index++) {
    const bool last_char = (index < strlen(uart_driver_returned_data));
    uart__get_ExpectAndReturn(UART__3, NULL, 0, last_char);
    uart__get_IgnoreArg_input_byte();
    if (last_char) {
      uart__get_ReturnThruPtr_input_byte(&uart_driver_returned_data[index]);
    }
  }
  gps__run_once();
  parsed_coordinates = gps__get_coordinates();
  TEST_ASSERT_EQUAL_FLOAT(longitude_data_degrees, parsed_coordinates.latitude);
  TEST_ASSERT_EQUAL_FLOAT(latitude_data_degrees, parsed_coordinates.longitude);
}

void test_GPGGA_coordinates_are_parsed_negative_longitude(void) {
  char *uart_driver_returned_data =
      "$GPGGA,134658.00,5106.9792,S,11402.3003,E,2,09,1.0,1048.47,M,-16.27,M,08,AAAA*60\n";
  float longitude_data = -5106.9792;
  float longitude_data_degrees = -51.11632;
  float latitude_data = 11402.3003;
  float latitude_data_degrees = 114.0383;
  gps_coordinates_t parsed_coordinates;
  gpio_s switchs;
  gpio_s led = {};
  gpio_s data_ready = {GPIO__PORT_0, 6};

  test_init();

  for (size_t index = 0; index <= strlen(uart_driver_returned_data); index++) {
    const bool last_char = (index < strlen(uart_driver_returned_data));
    uart__get_ExpectAndReturn(UART__3, NULL, 0, last_char);
    uart__get_IgnoreArg_input_byte();
    if (last_char) {
      uart__get_ReturnThruPtr_input_byte(&uart_driver_returned_data[index]);
    }
  }
  gps__run_once();
  // verify_data
  parsed_coordinates = gps__get_coordinates();
  TEST_ASSERT_EQUAL_FLOAT(longitude_data_degrees, parsed_coordinates.latitude);
  TEST_ASSERT_EQUAL_FLOAT(latitude_data_degrees, parsed_coordinates.longitude);
}

void test_GPGGA_coordinates_are_parsed_negative_latitude(void) {
  char *uart_driver_returned_data =
      "$GPGGA,134658.00,5106.9792,N,11402.3003,W,2,09,1.0,1048.47,M,-16.27,M,08,AAAA*60\n";
  float longitude_data = 5106.9792;
  float longitude_data_degrees = 51.11632;
  float latitude_data = -11402.3003;
  float latitude_data_degrees = -114.0383;
  gps_coordinates_t parsed_coordinates;
  gpio_s switchs;
  gpio_s led = {};
  gpio_s data_ready = {GPIO__PORT_0, 6};
  test_init();

  for (size_t index = 0; index <= strlen(uart_driver_returned_data); index++) {
    const bool last_char = (index < strlen(uart_driver_returned_data));
    uart__get_ExpectAndReturn(UART__3, NULL, 0, last_char);
    uart__get_IgnoreArg_input_byte();
    if (last_char) {
      uart__get_ReturnThruPtr_input_byte(&uart_driver_returned_data[index]);
    }
  }
  gps__run_once();
  // verify_data
  parsed_coordinates = gps__get_coordinates();
  TEST_ASSERT_EQUAL_FLOAT(longitude_data_degrees, parsed_coordinates.latitude);
  TEST_ASSERT_EQUAL_FLOAT(latitude_data_degrees, parsed_coordinates.longitude);
}

void test_GPGGA_incomplete_line(void) {
  char *uart_driver_returned_data = "$GPGGA,134658.00";
  float longitude_data = 0;
  float latitude_data = 0;
  gps_coordinates_t parsed_coordinates = {0, 0};
  gpio_s switchs;
  gpio_s led = {};
  gpio_s data_ready = {GPIO__PORT_0, 6};
  test_init();

  for (size_t index = 0; index <= strlen(uart_driver_returned_data); index++) {
    const bool last_char = (index < strlen(uart_driver_returned_data));
    uart__get_ExpectAndReturn(UART__3, NULL, 0, last_char);
    uart__get_IgnoreArg_input_byte();
    if (last_char) {
      uart__get_ReturnThruPtr_input_byte(&uart_driver_returned_data[index]);
    }
  }

  gps__run_once();
  // verify_data
  parsed_coordinates = gps__get_coordinates();
  TEST_ASSERT_EQUAL_FLOAT(longitude_data, parsed_coordinates.latitude);
  TEST_ASSERT_EQUAL_FLOAT(latitude_data, parsed_coordinates.longitude);
}

void test_GPGGA_incomplete_line_but_has_coordinates(void) {
  char *uart_driver_returned_data = "$GPGGA,134658.00,5106.9792,N,11402.3003,W,2,09,1.0,1048.47";
  float longitude_data = 0;
  float latitude_data = 0;
  gps_coordinates_t parsed_coordinates = {0, 0};
  gpio_s switchs;
  gpio_s led = {};
  gpio_s data_ready = {GPIO__PORT_0, 6};
  test_init();

  for (size_t index = 0; index <= strlen(uart_driver_returned_data); index++) {
    const bool last_char = (index < strlen(uart_driver_returned_data));
    uart__get_ExpectAndReturn(UART__3, NULL, 0, last_char);
    uart__get_IgnoreArg_input_byte();
    if (last_char) {
      uart__get_ReturnThruPtr_input_byte(&uart_driver_returned_data[index]);
    }
  }

  gps__run_once();

  // verify_data
  parsed_coordinates = gps__get_coordinates();
  TEST_ASSERT_EQUAL_FLOAT(longitude_data, parsed_coordinates.latitude);
  TEST_ASSERT_EQUAL_FLOAT(latitude_data, parsed_coordinates.longitude);
}
