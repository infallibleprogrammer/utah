"""
SCons command line interface
"""

from SCons.Script import *
import os

project_name = (os.environ.get('current_board'))
print(project_name)
if(project_name is None):
    print("LINUX environment variable, current_board, not defined. Assign please\n")
    exit()
elif (project_name == 'GEO_Controller' or project_name == 'Master_Controller' or project_name == 'Motor_Controller' or project_name == 'Sensor_Controller'):
    print("Current Board")
else:
    print("Unexpected environment variable, current_board\n")
    exit()

def cli_init():
    AddOption("--dbc-node-name", default=None)
    AddOption("--no-clang-format", action="store_true", default=False)
    AddOption("--no-float-format", action="store_true", default=False)
    AddOption("--no-unit-test", action="store_true", default=False)
    AddOption("--project", metavar="<project directory name>", default=project_name, help="Specify a target project directory to build")
    AddOption("--test-output", action="store_true", default=False)
    AddOption("--timeout", type=int, default=5, help="Unit test timeout in seconds")
    AddOption("--verbose", action="store_true", default=False)
