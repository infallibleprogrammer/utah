/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/
/* Main Module Header */
#include "rpm_sensor.h"

/* Standard Includes */
#include <stddef.h>

/* External Includes */
#include "clock.h"
#include "gpio.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"

/***********************************************************************************************************************
 *
 *                                  P R I V A T E   D A T A   D E F I N I T I O N S
 *
 **********************************************************************************************************************/
static const uint32_t timer_counter_upper_bound = 10000;

static const float circumference_of_wheel_cm = 33.913f;
static const float wheel_to_gear_ratio = 2.125f;
static float kph_scalar = (circumference_of_wheel_cm * 3600) / (wheel_to_gear_ratio * 100000);

static float speed_in_kph;

/***********************************************************************************************************************
 *
 *                                         P R I V A T E   F U N C T I O N S
 *
 **********************************************************************************************************************/
/**
 * @brief ISR used to reset TC
 */
static void timer2_isr(void) {
  LPC_TIM2->TC = 0;
  LPC_TIM2->IR |= 0x10;
}

/***********************************************************************************************************************
 *
 *                                          P U B L I C   F U N C T I O N S
 *
 **********************************************************************************************************************/
/**
 * @note Using prescalar of 959, thus
 */
void rpm_sensor__init(void) {

  uint32_t prescalar_divider =
      959; // 96MHz / 960 results in 10us ticks, which is almost 12 hours for wrap back (experimenting)

  lpc_peripheral__turn_on_power_to(LPC_PERIPHERAL__TIMER2);

  LPC_TIM2->IR |= 0x3F;
  LPC_TIM2->CCR = (1 << 0) | (1 << 2); // TC captured in CR0 on rising edge and interrupt is generated

  lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__TIMER2, timer2_isr, NULL);

  LPC_TIM2->PR = prescalar_divider;
  LPC_TIM2->TCR |= (1 << 0);

  gpio__construct_with_function(GPIO__PORT_2, 6, GPIO__FUNCTION_3);

  // setting local globals
  kph_scalar *= clock__get_peripheral_clock_hz() / (prescalar_divider + 1);
}

void rpm_sensor__update_speed_value(void) {

  uint32_t timer_capture_value = LPC_TIM2->CR0;
  uint32_t timer_counter_value = LPC_TIM2->TC;

  /* Check if car is probably stopped */
  if (timer_counter_value > timer_counter_upper_bound || timer_capture_value == 0) {
    speed_in_kph = 0.0f;
    return;
  }

  speed_in_kph = kph_scalar / timer_capture_value;
}

float rpm_sensor__get_speed(void) { return speed_in_kph; }
