#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockboard_io.h"
#include "Mockgpio.h"

#include "Mockcan_bus_handler.h"
#include "Mockdc_motor.h"
#include "Mockmotor_logic.h"
#include "Mockrpm_sensor.h"
#include "Mockservo_motor.h"

// Include the source we wish to test
#include "periodic_callbacks.h"

static gpio_s led0 = {.port_number = GPIO__PORT_2, .pin_number = 3};
static gpio_s led1 = {.port_number = GPIO__PORT_1, .pin_number = 26};
static gpio_s led2 = {.port_number = GPIO__PORT_1, .pin_number = 24};
static gpio_s led3 = {.port_number = GPIO__PORT_1, .pin_number = 18};

void setUp(void) {}

void tearDown(void) {}

void test__periodic_callbacks__initialize(void) {
  can_bus_handler__init_Expect();
  dc_motor__init_Expect();
  servo_motor__init_Expect();
  rpm_sensor__init_Expect();

  board_io__get_led0_ExpectAndReturn(led0);
  board_io__get_led1_ExpectAndReturn(led1);
  board_io__get_led2_ExpectAndReturn(led2);
  board_io__get_led3_ExpectAndReturn(led3);

  gpio__set_Expect(led0);
  gpio__set_Expect(led1);
  gpio__set_Expect(led2);
  gpio__set_Expect(led3);

  periodic_callbacks__initialize();
}
