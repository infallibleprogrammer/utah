/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/
/* Main Module Header */

/* Standard Includes */
#include <math.h>
#include <stdio.h>

/* External Includes */
#include "unity.h"

#include "Mockgpio.h"
#include "Mockpwm1.h"

#include "dc_motor.h"

/***********************************************************************************************************************
 *
 *                                  P R I V A T E   D A T A   D E F I N I T I O N S
 *
 **********************************************************************************************************************/
static gpio_s P2_1 = {.port_number = GPIO__PORT_2, .pin_number = 1};
static uint32_t exptected_pwm_frequency = 100;
static pwm1_channel_e expected_pwm1_channel = PWM1__2_1;

static const float min_duty_cycle = 13.0f; // speed limiting for now
static const float max_duty_cycle = 17.0f;
static const float idle_duty_cycle = 15.0f; // also the offset

static const float min_drive_speed = -10.0f;
static const float max_drive_speed = 10.0f;

static const float degree_to_duty_scalar = (max_duty_cycle - min_duty_cycle) / (max_drive_speed - min_drive_speed);

/***********************************************************************************************************************
 *
 *                                         P R I V A T E   F U N C T I O N S
 *
 **********************************************************************************************************************/
static float speed_limit(float speed) {
  return speed < min_drive_speed ? min_drive_speed : speed > max_drive_speed ? max_drive_speed : speed;
}

/***********************************************************************************************************************
 *
 *                                          P U B L I C   F U N C T I O N S
 *
 **********************************************************************************************************************/
void setUp(void) {}

void tearDown(void) {}

void test__dc_motor__init(void) {
  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_2, 1, GPIO__FUNCTION_1, P2_1);
  pwm1__init_single_edge_Expect(exptected_pwm_frequency);
  pwm1__set_duty_cycle_Expect(expected_pwm1_channel, idle_duty_cycle);

  dc_motor__init();
}

void test__dc_motor__change_drive_speed__0_to_10kph(void) {

  float drive_speed = speed_limit(10.0f);
  float target_duty_cycle_percent = drive_speed * degree_to_duty_scalar + idle_duty_cycle;
  pwm1__set_duty_cycle_Expect(expected_pwm1_channel, target_duty_cycle_percent);

  dc_motor__change_drive_speed(drive_speed);
}

void test__dc_motor__change_drive_speed__10_to_5kph(void) {

  float drive_speed = speed_limit(5.0f);
  float target_duty_cycle_percent = drive_speed * degree_to_duty_scalar + idle_duty_cycle;
  pwm1__set_duty_cycle_Expect(expected_pwm1_channel, target_duty_cycle_percent);

  dc_motor__change_drive_speed(drive_speed);
}

void test__dc_motor__change_drive_speed__5_to_reverse_12kph(void) {

  float drive_speed = speed_limit(-12.0f);
  float target_duty_cycle_percent = drive_speed * degree_to_duty_scalar + idle_duty_cycle;
  pwm1__set_duty_cycle_Expect(expected_pwm1_channel, target_duty_cycle_percent);

  dc_motor__change_drive_speed(drive_speed);
}

void test__dc_motor__change_drive_speed__reverse_10_to_12kph(void) {

  float drive_speed = speed_limit(12.0f);
  float target_duty_cycle_percent = drive_speed * degree_to_duty_scalar + idle_duty_cycle;
  pwm1__set_duty_cycle_Expect(expected_pwm1_channel, target_duty_cycle_percent);

  dc_motor__change_drive_speed(drive_speed);
}