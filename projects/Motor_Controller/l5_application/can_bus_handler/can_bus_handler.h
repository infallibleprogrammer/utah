#pragma once
/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/
/* Standard Includes */
#include <stdbool.h>
#include <stdint.h>

/* External Includes */

/* Module Includes */
#include "project.h"

/**
 * @brief Return values for can_bus_handler__motor_rx
 */
static const uint8_t can_bus_handler__nothing_to_rx_flag = 0x00U;
static const uint8_t can_bus_handler__valid_rx_flag = 0x01U;
static const uint8_t can_bus_handler__invalid_rx_flag = 0xffU;

/***********************************************************************************************************************
 *
 *                                     F U N C T I O N   D E C L A R A T I O N S
 *
 **********************************************************************************************************************/

// initialise CAN bus
void can_bus_handler__init(void);

void can_bus_handler__reset_motor_mia_counter(void);

/**
 * @brief  Receive packets from CAN bus related to motor controller.
 * @param  callback_rate: Periodic callback enumeration to set CAN timeout modularly
 * @param  drive_speed: Address to write drive speed value
 * @param  steer_angle: Address to write steer angle value
 * @retval 0x00 when there is no data to receive
 *         0x01 on valid drive speed and steer angle data
 *         0xff when data is invalid, i.e. the message is not of interest to this controller.
 */
uint8_t can_bus_handler__motor_rx(float *drive_speed, int8_t *steer_angle);

bool can_bus_handler__rx_all_msgs(float *drive_speed, int8_t *steer_angle);

/**
 * @brief  Tx motor messages
 * @param  callback_rate: Periodic callback enumeration to set CAN timeout modularly
 * @param  rc_car_speed: Current measurement of RC car
 *                       speed.
 * @retval True on success, false otherwise.
 */
bool can_bus_handler__motor_speed_tx(float rc_car_speed);
bool can_bus_handler__motor_heartbeat_tx(void);

// reset CAN bus
bool can_bus_handler__reset_can_bus_if_bus_goes_off(void);
