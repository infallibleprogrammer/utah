#pragma once

#include "project.h"

dbc_GPS_DESTINATION_LOCATION_s *fake_gps_data(void);
dbc_GPS_DESTINATION_LOCATION_s *fake_gps_data_incremental(void);
void fake_gps_init(void);