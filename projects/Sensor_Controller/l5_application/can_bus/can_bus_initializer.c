#include "can_bus_initializer.h"
#include "can_bus.h"
#include <stddef.h>

bool initiailize_can1_bus(uint32_t baud_rate, uint32_t rxq_size, uint32_t txq_size) {
  bool return_var = false;
  if (can__init(can1, baud_rate, rxq_size, txq_size, NULL, NULL)) {
    return_var = true;
  }

  can__bypass_filter_accept_all_msgs();

  // leaving for future hardware sorting, need to identify what is wrong
  // const can_std_id_t slist[] = {can__generate_standard_id(can1, 200)};
  // const can_std_grp_id_t *sglist = NULL;
  // const can_ext_id_t *elist = NULL; // Not used, so set it to NULL
  // const can_ext_grp_id_t *eglist = NULL;
  // can__setup_filter(slist, 1, sglist, 0, elist, 0, eglist, 0);

  can__reset_bus(can1);
  return return_var;
}

void initiailize_can2_bus(uint32_t baud_rate, uint32_t rxq_size, uint32_t txq_size) {
  can__init(can2, baud_rate, rxq_size, txq_size, NULL, NULL);
  can__bypass_filter_accept_all_msgs();
  can__reset_bus(can2);
}
