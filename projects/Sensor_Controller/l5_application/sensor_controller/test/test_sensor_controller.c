
#include <stdio.h>
#include <string.h>

#include "unity.h"

#include "Mockboard_io.h"
#include "Mockcan_bus.h"
#include "Mockgpio.h"

#include "Mockcan_bus_initializer.h"
#include "can_dbc_send_msg.c"

#include "Mockadc.h"
#include "Mockultra_sonic_sensors.h"
#include "project.h"
#include "sensor_controller.c"

void setUp(void) {}

void tearDown(void) {}

void test__Sensor_Controller_init(void) {
  initialize_adc_for_ultra_sonic_sensors_Expect();
  initialize_pins_for_ultra_sonic_sensor_triggers_Expect();
  Sensor_Controller_init();
}

void test__Sensor_Controller__100hz_handler(void) {
  uint16_t front_sensor_median, back_sensor_median, left_sensor_median, right_sensor_median;

  fill_left_and_right_ultra_sonic_distance_buffer_Expect();
  sort_ultra_sonic_distances_Expect(LEFT_ULTRA_SONIC);
  sort_ultra_sonic_distances_Expect(RIGHT_ULTRA_SONIC);
  Sensor_Controller__100hz_handler(1);

  fill_front_and_back_ultra_sonic_distance_buffer_Expect();
  sort_ultra_sonic_distances_Expect(FRONT_ULTRA_SONIC);
  sort_ultra_sonic_distances_Expect(BACK_ULTRA_SONIC);
  Sensor_Controller__100hz_handler(4);

  get_median_distance_ExpectAndReturn(FRONT_ULTRA_SONIC, front_sensor_median);
  get_median_distance_ExpectAndReturn(BACK_ULTRA_SONIC, back_sensor_median);
  get_median_distance_ExpectAndReturn(LEFT_ULTRA_SONIC, left_sensor_median);
  get_median_distance_ExpectAndReturn(RIGHT_ULTRA_SONIC, right_sensor_median);
  can__tx_ExpectAndReturn(can1, &tx_msg, 0, true);
  Sensor_Controller__100hz_handler(5);
}
