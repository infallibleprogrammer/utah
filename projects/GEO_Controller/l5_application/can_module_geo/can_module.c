#include "can_module.h"
#include "can_bus.h"
#include "compass.h"
#include "geo_logic.h"
#include "gps.h"
#include "project.h"

static dbc_DBG_GEO_CAN_STATUS_s get_can_bus_status(void) {
  dbc_DBG_GEO_CAN_STATUS_s can_bus_status;
  can_bus_status.DBG_CAN_INIT_STATUS = !(can__is_bus_off(can1));
  can_bus_status.DBG_CAN_RX_DROP_COUNT = can__get_rx_dropped_count(can1);
  return can_bus_status;
}

void can_bus_handler__process_all_received_messages(void) {
  can__msg_t can_msg = {0};
  dbc_GPS_DESTINATION_LOCATION_s BRIDGE_TO_GPS_DATA = {};

  // Receive all messages
  while (can__rx(can1, &can_msg, 5)) {
    const dbc_message_header_t header = {
        .message_id = can_msg.msg_id,
        .message_dlc = can_msg.frame_fields.data_len,
    };

    // If sensor data was successfully decoded, hand it over to the driver logic
    if (dbc_decode_GPS_DESTINATION_LOCATION(&BRIDGE_TO_GPS_DATA, header, can_msg.data.bytes)) {
      geo_controller_process_GEO_data(&BRIDGE_TO_GPS_DATA);
    }
  }
}

void periodic_callbacks_10Hz_GEO_commands(void) {
  const dbc_GPS_CURRENT_INFO_s current_RC_location = geo_controller_process_GEO_current_location();
  const dbc_GPS_CURRENT_DESTINATIONS_DATA_s current_destination_data = determine_destination_location();
  const dbc_COMPASS_HEADING_DISTANCE_s current_compass_data = determine_compass_heading_and_distance();
  const dbc_GPS_COMPASS_STATUS_s GPS_compass_status = check_GPS_Compass_Status();
  //   const dbc_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG_s command = driver__get_motor_commands();

  // send current destination information
  can__msg_t can_msg = {};
  const dbc_message_header_t gps_destination_header =
      dbc_encode_GPS_CURRENT_DESTINATIONS_DATA(can_msg.data.bytes, &current_destination_data);
  can_msg.msg_id = gps_destination_header.message_id;
  can_msg.frame_fields.data_len = gps_destination_header.message_dlc;
  can__tx(can1, &can_msg, 0);

  // send heading and distance

  const dbc_message_header_t heading_and_distance_information =
      dbc_encode_COMPASS_HEADING_DISTANCE(can_msg.data.bytes, &current_compass_data);
  can_msg.msg_id = heading_and_distance_information.message_id;
  can_msg.frame_fields.data_len = heading_and_distance_information.message_dlc;
  can__tx(can1, &can_msg, 0);

  const dbc_message_header_t gps_compass_status_header =
      dbc_encode_GPS_COMPASS_STATUS(can_msg.data.bytes, &GPS_compass_status);
  can_msg.msg_id = gps_compass_status_header.message_id;
  can_msg.frame_fields.data_len = gps_compass_status_header.message_dlc;
  can__tx(can1, &can_msg, 0);

  // send rc location to bridge
  const dbc_message_header_t current_RC_location_header =
      dbc_encode_GPS_CURRENT_INFO(can_msg.data.bytes, &current_RC_location);
  can_msg.msg_id = current_RC_location_header.message_id;
  can_msg.frame_fields.data_len = current_RC_location_header.message_dlc;
  can__tx(can1, &can_msg, 0);
}

void periodic_callbacks_1Hz_GEO_commands(void) {
  const dbc_DBG_GEO_CAN_STATUS_s geo_can_status = get_can_bus_status();
  const dbc_DBG_GPS_COMPASS_LOCK_LED_CHECK_s GEO_LOCK_LED = check_GPS_COMPASS_LED();
  const dbc_DBG_CONFIRM_RECEIVED_DESTINATION_s received_location = confirm_last_added_destination().status_values;
  const dbc_DBG_RAW_COMPASS_DATA_s raw_compass_data = sent_RAW_COMPASS_REGISTER();
  // send current destination information
  can__msg_t can_msg = {};
  const dbc_message_header_t dbg_confirmed_received_destination =
      dbc_encode_DBG_CONFIRM_RECEIVED_DESTINATION(can_msg.data.bytes, &received_location);
  can_msg.msg_id = dbg_confirmed_received_destination.message_id;
  can_msg.frame_fields.data_len = dbg_confirmed_received_destination.message_dlc;
  if (can__tx(can1, &can_msg, 0)) {
    processed_compass_gps_data();
  }

  // send heading and distance

  const dbc_message_header_t geo_can_status_header = dbc_encode_DBG_GEO_CAN_STATUS(can_msg.data.bytes, &geo_can_status);
  can_msg.msg_id = geo_can_status_header.message_id;
  can_msg.frame_fields.data_len = geo_can_status_header.message_dlc;
  can__tx(can1, &can_msg, 0);

  // send rc location to bridge
  const dbc_message_header_t geo_lock_LED_header =
      dbc_encode_DBG_GPS_COMPASS_LOCK_LED_CHECK(can_msg.data.bytes, &GEO_LOCK_LED);
  can_msg.msg_id = geo_lock_LED_header.message_id;
  can_msg.frame_fields.data_len = geo_lock_LED_header.message_dlc;
  can__tx(can1, &can_msg, 0);

  const dbc_message_header_t raw_compass_data_header =
      dbc_encode_DBG_RAW_COMPASS_DATA(can_msg.data.bytes, &raw_compass_data);
  can_msg.msg_id = raw_compass_data_header.message_id;
  can_msg.frame_fields.data_len = raw_compass_data_header.message_dlc;
  can__tx(can1, &can_msg, 0);
}
