#pragma once
/***********************************************************************************************************************
 *
 *                                     F U N C T I O N   D E C L A R A T I O N S
 *
 **********************************************************************************************************************/

/**
 * @brief Initialize the servo motor
 */
void servo_motor__init(void);

/**
 * @brief Changes steering angle of RC car where:
 *        -45 is full left
 *          0 is center
 *         45 is full right
 */
void servo_motor__change_steering_angle(float steering_angle);
