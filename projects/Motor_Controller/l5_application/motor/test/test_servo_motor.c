/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/
/* Main Module Header */

/* Standard Includes */
#include <stdio.h>

/* External Includes */
#include "unity.h"

#include "Mockgpio.h"
#include "Mockpwm1.h"

#include "servo_motor.h"

/***********************************************************************************************************************
 *
 *                                  P R I V A T E   D A T A   D E F I N I T I O N S
 *
 **********************************************************************************************************************/
static gpio_s P2_4 = {.port_number = GPIO__PORT_2, .pin_number = 4};
static uint32_t exptected_pwm_frequency = 100;
static pwm1_channel_e expected_pwm1_channel = PWM1__2_4;

static const float min_duty_cycle = 10.0f;
static const float max_duty_cycle = 20.0f;
static const float idle_duty_cycle = 15.0f; // also the offset

static const float min_steer_angle = -45.0f;
static const float max_steer_angle = 45.0f;

static const float degree_to_duty_scalar = (max_duty_cycle - min_duty_cycle) / (max_steer_angle - min_steer_angle);

/***********************************************************************************************************************
 *
 *                                         P R I V A T E   F U N C T I O N S
 *
 **********************************************************************************************************************/
static float angle_limit(float angle) {
  return angle < min_steer_angle ? min_steer_angle : angle > max_steer_angle ? max_steer_angle : angle;
}
/***********************************************************************************************************************
 *
 *                                          P U B L I C   F U N C T I O N S
 *
 **********************************************************************************************************************/
void setUp(void) {}

void tearDown(void) {}

void test__servo_motor__init(void) {
  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_2, 4, GPIO__FUNCTION_1, P2_4);
  pwm1__init_single_edge_Expect(exptected_pwm_frequency);
  pwm1__set_duty_cycle_Expect(expected_pwm1_channel, idle_duty_cycle);

  servo_motor__init();
}

void test__servo_motor__change_steering_angle__0_to_50deg(void) {

  float steer_angle = angle_limit(50);
  float desired_duty_cycle_percent = steer_angle * degree_to_duty_scalar + idle_duty_cycle;
  pwm1__set_duty_cycle_Expect(expected_pwm1_channel, desired_duty_cycle_percent);
  servo_motor__change_steering_angle(steer_angle);
}

void test__servo_motor__change_steering_angle__45_to_10deg(void) {

  float steer_angle = angle_limit(10);
  float desired_duty_cycle_percent = steer_angle * degree_to_duty_scalar + idle_duty_cycle;
  pwm1__set_duty_cycle_Expect(expected_pwm1_channel, desired_duty_cycle_percent);
  servo_motor__change_steering_angle(steer_angle);
}

void test__servo_motor__change_steering_angle__10_to_neg_50deg(void) {

  float steer_angle = angle_limit(-50);
  float desired_duty_cycle_percent = steer_angle * degree_to_duty_scalar + idle_duty_cycle;
  pwm1__set_duty_cycle_Expect(expected_pwm1_channel, desired_duty_cycle_percent);
  servo_motor__change_steering_angle(steer_angle);
}

void test__servo_motor__change_steering_angle__neg_45_to_0deg(void) {

  float steer_angle = angle_limit(0);
  float desired_duty_cycle_percent = steer_angle * degree_to_duty_scalar + idle_duty_cycle;
  pwm1__set_duty_cycle_Expect(expected_pwm1_channel, desired_duty_cycle_percent);
  servo_motor__change_steering_angle(steer_angle);
}
