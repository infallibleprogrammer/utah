#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockcan_bus.h"
#include "Mockdriver_logic.h"

#include "can_mia_integration.c"
#include "can_module.h"
#include "project.h"

void test_can_bus_handler__process_all_received_messages_SENSOR_SONARS_ROUTINE_decoded_successfully(void) {
  can__msg_t can_msg = {0};
  dbc_SENSOR_SONARS_ROUTINE_s sensor_data = {};

  can__rx_ExpectAndReturn(can1, &can_msg, 0, false); // All messages receieved
  can__rx_IgnoreArg_can_message_ptr();

  const dbc_message_header_t header = {
      .message_id = can_msg.msg_id = 102U,
      .message_dlc = can_msg.frame_fields.data_len = 5,
  };

  TEST_ASSERT_TRUE(dbc_decode_SENSOR_SONARS_ROUTINE(&sensor_data, header, can_msg.data.bytes));
  // driver__process_input_Expect(&sensor_data);

  can_bus_handler__process_all_sensor_messages();
}

void test_can_bus_handler__process_all_received_messages_SENSOR_SONARS_ROUTINE_decoded_failure(void) {
  can__msg_t can_msg = {0};
  dbc_SENSOR_SONARS_ROUTINE_s sensor_data = {};

  can__rx_ExpectAndReturn(can1, &can_msg, 0, false); // All messages receieved
  can__rx_IgnoreArg_can_message_ptr();

  const dbc_message_header_t header = {
      .message_id = can_msg.msg_id = 14U,
      .message_dlc = can_msg.frame_fields.data_len = 5,
  };

  TEST_ASSERT_FALSE(dbc_decode_SENSOR_SONARS_ROUTINE(&sensor_data, header, can_msg.data.bytes));

  can_bus_handler__process_all_sensor_messages();
}
void test_can_bus_handler__process_all_received_messages_COMPASS_HEADING_DISTANCE_decoded_successfully(void) {
  can__msg_t can_msg = {0};
  dbc_SENSOR_SONARS_ROUTINE_s geo_data = {};

  can__rx_ExpectAndReturn(can1, &can_msg, 0, false); // All messages receieved
  can__rx_IgnoreArg_can_message_ptr();

  const dbc_message_header_t header = {
      .message_id = can_msg.msg_id = 301U,
      .message_dlc = can_msg.frame_fields.data_len = 6,
  };
  TEST_ASSERT_TRUE(dbc_decode_COMPASS_HEADING_DISTANCE(&geo_data, header, can_msg.data.bytes));

  can_bus_handler__process_all_geo_messages();
}
void test_can_bus_handler__process_all_received_messages_COMPASS_HEADING_DISTANCE_decoded_failure(void) {
  can__msg_t can_msg = {0};
  dbc_SENSOR_SONARS_ROUTINE_s geo_data = {};

  can__rx_ExpectAndReturn(can1, &can_msg, 0, false); // All messages receieved
  can__rx_IgnoreArg_can_message_ptr();

  const dbc_message_header_t header = {
      .message_id = can_msg.msg_id = 901U,
      .message_dlc = can_msg.frame_fields.data_len = 8,
  };
  TEST_ASSERT_FALSE(dbc_decode_COMPASS_HEADING_DISTANCE(&geo_data, header, can_msg.data.bytes));

  can_bus_handler__process_all_geo_messages();
}
void test_periodic_callbacks_10Hz_motor_commands(void) {
  dbc_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG_s command;
  driver__get_motor_commands_ExpectAndReturn(command);

  can__msg_t can_msg = {};
  const dbc_message_header_t header = dbc_encode_MOTOR_CHANGE_SPEED_AND_ANGLE_MSG(can_msg.data.bytes, &command);

  can_msg.msg_id = header.message_id;
  can_msg.frame_fields.data_len = header.message_dlc;

  can__tx_ExpectAndReturn(can1, &can_msg, 0, true);
  can__tx_IgnoreArg_can_message_ptr();
  periodic_callbacks_10Hz_motor_commands();
}