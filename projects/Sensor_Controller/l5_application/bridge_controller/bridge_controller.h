#pragma once

#include "can_dbc_send_msg.h"
#include "project.h"
#include "sensor_controller.h"
#include <stdbool.h>
#include <stdint.h>

void Bridge_Controller_init(void);

bool can__Bridge_Controller__coordinate_send(void);

void Bridge_Controller__1hz_handler(void);
void Bridge_Controller__10hz_handler(void);
void can_Bridge_Controller__manage_mia_msgs_10hz(void);
bool Bridge_Controller__get_coordinates(dbc_GPS_DESTINATION_LOCATION_s *gps_send_data);

static void Bridge_Controller__transfer_data_from_uart_driver_to_line_buffer(void);
static void Bridge_Controller__get_line_from_buffer(void);
static void Bridge_Controller__print_line(void);
static bool can_Bridge_Controller__RX_and_send_to_app_Msg(void);
static void Bridge_Controller__Bluetooth_Send_uint16(uint16_t uint_out, uint32_t command);
static void Bridge_Controller__Bluetooth_Send_uint32(uint32_t uint_out, uint32_t command);
static void Bridge_Controller__Bluetooth_Send_float(float float_out, uint32_t msg_identifier);

static void Bridge_Controller__get_sensor_data(void);
