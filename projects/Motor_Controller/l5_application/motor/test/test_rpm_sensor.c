/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/
/* Main Module Header */

/* Standard Includes */
#include <stdio.h>

/* External Includes */
#include "unity.h"

#include "Mockclock.h"
#include "Mockgpio.h"
#include "Mocklpc_peripherals.h"

#include "rpm_sensor.h"

/***********************************************************************************************************************
 *
 *                                  P R I V A T E   D A T A   D E F I N I T I O N S
 *
 **********************************************************************************************************************/
static gpio_s P2_6 = {.port_number = GPIO__PORT_2, .pin_number = 6};

/***********************************************************************************************************************
 *
 *                                          P U B L I C   F U N C T I O N S
 *
 **********************************************************************************************************************/
void setUp(void) {}

void tearDown(void) {}

/**
 * @note The init test is still creating problems when testing because of writing to hardware registers.
 */
// void test__rpm_sensor__init(void) {
//   lpc_peripheral__turn_on_power_to_Expect(LPC_PERIPHERAL__TIMER2);
//   lpc_peripheral__enable_interrupt_Expect(LPC_PERIPHERAL__TIMER2, NULL, NULL);
//   lpc_peripheral__enable_interrupt_IgnoreArg_isr_callback();
//   gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_2, 6, GPIO__FUNCTION_3, P2_6);
//   clock__get_peripheral_clock_hz_ExpectAndReturn(96000000);

//   rpm_sensor__init();
// }

/**
 * TODO: Test conversion.  I'm stuck on how to simulate the TIM2 registers, and thus
 *       moving on to other things.
 */
