/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/
/* Main Module Header */
#include "motor_logic.h"

/* Standard Includes */

/* External Includes */
#include "dc_motor.h"
#include "motor_pid.h"
#include "rpm_sensor.h"
#include "servo_motor.h"

/***********************************************************************************************************************
 *
 *                                  P R I V A T E   D A T A   D E F I N I T I O N S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                          P U B L I C   F U N C T I O N S
 *
 **********************************************************************************************************************/
bool motor_logic__self_test(void) {
  static unsigned int test_count;
  bool done = false;

  if (test_count == 0) {

    servo_motor__change_steering_angle(-45);
    ++test_count;

  } else if (test_count == 1) {

    servo_motor__change_steering_angle(45);
    ++test_count;

  } else if (test_count == 2) {

    servo_motor__change_steering_angle(0);
    ++test_count;

  } else if (test_count == 3) {

    dc_motor__change_drive_speed(-5);
    ++test_count;

  } else if (test_count == 4) {

    dc_motor__change_drive_speed(0);
    ++test_count;

  } else if (test_count == 5) {

    dc_motor__change_drive_speed(5);
    ++test_count;

  } else if (test_count == 6) {

    dc_motor__change_drive_speed(0);
    done = true;
  }

  return done;
}

void motor_logic__update_motor_state(float target_speed) {
  rpm_sensor__update_speed_value();

  if (target_speed > 0) {
    next_drive_direction = DRIVE_FORWARD;
  } else if (target_speed < 0) {
    next_drive_direction = DRIVE_REVERSE;
  } else {
    next_drive_direction = DRIVE_NEUTRAL;
  }

  switch (next_drive_direction) {
  case DRIVE_FORWARD:
    if (current_drive_direction != DRIVE_FORWARD) {
      if (current_drive_direction == DRIVE_NEUTRAL) {
        dc_motor__change_drive_speed(target_speed);
        current_drive_direction = DRIVE_FORWARD;
      } else {
        dc_motor__change_drive_speed(0);
        current_drive_direction = DRIVE_NEUTRAL;
      }
    } else {
      // I'm not sure I'm doing this right.
      double update_speed = target_speed + motor_pid__compute_pid(target_speed, rpm_sensor__get_speed());
      dc_motor__change_drive_speed(update_speed);
    }
    break;

  case DRIVE_NEUTRAL:
    dc_motor__change_drive_speed(0);
    current_drive_direction = DRIVE_NEUTRAL;
    break;

  case DRIVE_REVERSE:
    if (current_drive_direction != DRIVE_REVERSE) {
      if (current_drive_direction == DRIVE_NEUTRAL) {
        dc_motor__change_drive_speed(target_speed);
        current_drive_direction = DRIVE_REVERSE;
      } else if (current_drive_direction == DRIVE_REVERSE0) {
        dc_motor__change_drive_speed(0);
        current_drive_direction = DRIVE_NEUTRAL;
      } else if (current_drive_direction == DRIVE_NEUTRAL0) {
        dc_motor__change_drive_speed(target_speed);
        current_drive_direction = DRIVE_REVERSE0;
      } else {
        dc_motor__change_drive_speed(0);
        current_drive_direction = DRIVE_NEUTRAL0;
      }
    } else {
      double update_speed = target_speed + motor_pid__compute_pid(target_speed, rpm_sensor__get_speed());
      dc_motor__change_drive_speed(update_speed);
    }
    break;

  default:
    dc_motor__change_drive_speed(0);
    current_drive_direction = DRIVE_NEUTRAL;
    break;
  }
}
