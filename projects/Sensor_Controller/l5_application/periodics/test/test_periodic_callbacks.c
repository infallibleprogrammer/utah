#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockboard_io.h"
#include "Mockgpio.h"

// Include the source we wish to test
#include "Mockadc.h"
#include "Mockbridge_controller.h"
#include "Mockcan_bus_initializer.h"
#include "Mockcan_dbc_send_msg.h"
#include "Mocksensor_controller.h"
#include "Mockultra_sonic_sensors.h"
#include "periodic_callbacks.h"

void setUp(void) {}

void tearDown(void) {}

void test__periodic_callbacks__initialize(void) {
  // initiailize_can1_bus_ExpectAndReturn(100, 100, 100, true);
  // Sensor_Controller_init_Expect();
  // periodic_callbacks__initialize();
}

void test__periodic_callbacks__1Hz__should_call_Bridge_Controller__1hz_handler(void) {

  Bridge_Controller__1hz_handler_Expect();
  periodic_callbacks__1Hz(0);
}

void test__periodic_callbacks__10Hz__should_call_Bridge_Controller__10hz_handler(void) {
  can_Bridge_Controller__manage_mia_msgs_10hz_Expect();
  Bridge_Controller__10hz_handler_Expect();
  periodic_callbacks__10Hz(0);
}

void test__periodic_callbacks__10Hz__should__call_Sensor_Controller__100hz_handler(void) {
  Sensor_Controller__100hz_handler_Expect(1);
  periodic_callbacks__100Hz(1);

  Sensor_Controller__100hz_handler_Expect(3);
  periodic_callbacks__100Hz(3);
}
